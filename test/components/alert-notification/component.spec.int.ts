import { default as test, Test } from "tape";
import * as TestUtil from "../../../test/util";
import * as FIXTURES from "../../../test/fixtures/components/alert-notification.js";

// Compiled component
const {default: AlertNotification} = require("../../../components/alert-notification");

const COMPONENT_JS_PATH = "app/components/alert-notification/component.js";

// TODO: FIX
// https://gitlab.com/mrman/vue-component-library/issues/12
// test("alert-notification component should $emit a destroy event when the x is clicked", (t: Test) => {

//   TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
//     .then(TestUtil.instantiateComponentFromLoadedGlobal("Component", FIXTURES.DEFAULT_ERROR))
//     .then((elemInfo: any) => {
//       const closeButton = elemInfo.elem.querySelector(".btn-close-alert");

//       // Watch for destroy to be emitted
//       elemInfo.vm.$on("destroy", (alert: any, alertIdx: number) => {
//         t.pass("destroy event emitted");

//         // Ensure that the $emit-ed destroy contains the alert and alert ID we expect
//         t.assert( FIXTURES.DEFAULT_ERROR.propsData.alert.should.eql(alert), "alerts hould be same as fixture data");
//         t.assert(
//           alertIdx == FIXTURES.DEFAULT_ERROR.propsData.alertIdx,
//           "alert index should be the same as fixture data (undefined for default)",
//         );

//         t.end();
//       });

//       // Click on the X, ensure it triggers a destroy,
//       TestUtil.jsdomClick(closeButton, elemInfo.window);
//     })
//     .catch(TestUtil.failAndEnd(t, "Destroy event not emitted"));

// });
