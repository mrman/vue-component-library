import Vue from "vue";
import { Component } from "vue";
import { default as test, Test } from "tape";
import * as TestUtil from "../../../test/util";
import * as FIXTURES from "../../../test/fixtures/components/alert-notification";

// Compiled component
const {default: AlertNotification} = require("../../../components/alert-notification");

// Define the systemjs path to the component code
const COMPONENT_JS_PATH = "components/alert-notification/component.js";

test("[alert-notification] default data is correct", (t: Test) => {
  t.plan(1);
  const vm: any = new Vue(AlertNotification).$mount();
  t.equals(vm.alert, undefined, "alert should be undefined");
});

test("[alert-notification] correct status class when created", (t: Test) => {
  t.plan(1);
  const vm: any = new Vue(AlertNotification).$mount();
  t.equals(vm.alertClass.trim(), "alert", "Alert class should be 'alert' if no message is specified");
});

// TODO: FIX
// https://gitlab.com/mrman/vue-component-library/issues/12
// test("alert-notification component properly server-side renders a single alert", (t: Test) => {

//   TestUtil.renderComponentWithData(Component, FIXTURES.CLOSABLE_ERROR)
//     .then((w: Window) => {

//       // Get the element and context
//       const e = w.document.getElementsByClassName("alert-notification-component")[0];
//       const content = e.textContent.trim();
//       const closeButton = e.querySelector(".btn-close-alert");

//       // Check content
//       t.assert(content.length > 0, "Content should be non-empty");
//       t.assert(closeButton !== null, "a close button should be rendered");
//       t.assert(
//         content.includes(FIXTURES.CLOSABLE_ERROR.propsData.alert.message),
//         "Expect to see the alert message in the content",
//       );

//       // Alert should be categorized properly
//       t.assert( Array.from(e.classList).includes("alert"), "classList should contain 'alert'");
//       t.assert( Array.from(e.classList).includes("alert-error"), "classList should contain 'alert-error'");

//     })
//     .then(t.end)
//     .catch(TestUtil.failAndEnd(t, "Destroy event not emitted"));
// });
