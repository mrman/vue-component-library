"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var vue_1 = __importDefault(require("vue"));
var tape_1 = __importDefault(require("tape"));
var AlertNotification = require("../../../components/alert-notification")["default"];
var COMPONENT_JS_PATH = "components/alert-notification/component.js";
tape_1["default"]("[alert-notification] default data is correct", function (t) {
    t.plan(1);
    var vm = new vue_1["default"](AlertNotification).$mount();
    t.equals(vm.alert, undefined, "alert should be undefined");
});
tape_1["default"]("[alert-notification] correct status class when created", function (t) {
    t.plan(1);
    var vm = new vue_1["default"](AlertNotification).$mount();
    t.equals(vm.alertClass.trim(), "alert", "Alert class should be 'alert' if no message is specified");
});
//# sourceMappingURL=component.spec.js.map