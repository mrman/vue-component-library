import Vue from "vue";
import getPort from "get-port";
import process from "process";
import rp from "request-promise-native";
import { GenericResponseEnvelope } from "../types";
import { Response, Options } from "request";
import { default as childProcess } from "child_process";
import { createRenderer } from "vue-server-renderer";
import { JSDOM, DOMWindow } from "jsdom";
import { remote, BrowserObject } from "webdriverio";
import { default as test, Test } from "tape";

const renderer = createRenderer();
const PROCESSES_TO_KILL: any[] = [];
const WAIT_BEFORE_KILL_AND_EXIT_MS = 500;

process.on("SIGINT", () => PROCESSES_TO_KILL.forEach(p => p.kill()));
process.on("exit", () => PROCESSES_TO_KILL.forEach(p => p.kill()));

// foce kill leftover processes when tape is finished
test.onFinish(() => {
  setTimeout(() => { // nexttick is not enough
    PROCESSES_TO_KILL.forEach((p: any) => p.kill());
  }, WAIT_BEFORE_KILL_AND_EXIT_MS);
});

export const defaultTestConfig = {timeout: 10000};

/**
 * Render a component with data provided
 *
 * @param {Object} Vue - The vue instance to use to render the component
 * @param {Object} Component - The component to render
 * @param {Object} props - The data to feed in to the component at init
 * @returns A promise that resolves to the `window` element of a DOM
 */
export function renderComponentWithData(Component: any, props: any): Promise<DOMWindow> {
  // Create component and mount
  const ModifiedVue = Vue.extend(Component);
  const vm = new ModifiedVue(props || {});

  return new Promise((resolve, reject) =>  {
    // Render the component
    renderer.renderToString(vm, (err: Error, html: string) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(new JSDOM(html).window);
    });
  });
}

/**
 * Render a module (usually a component) in JSDOM, by:
 * - Creating a JSPM builder
 * - Building a static bundle of the module
 * - Loading the bundle with JSDOM
 *
 * @param {string} path - Path to the module code
 * @param {string} name - The name to use for the name of the exported module
 * @returns A promise that resolves with the window of the generated jsdom environment
 */
export function renderModuleInJSDOM(path: string, name: string): Promise<DOMWindow> {
  // Make a static build of the module
  // TODO: This is almost certainly broken after JSPM is not being used anymore
  return Promise.resolve(
    new JSDOM(`<script name=${name} src=${path}></script>`, {runScripts: "dangerously"}).window,
  );
}

/**
 * Instantiate a component into an off-page element from a loaded global module in JSDOM
 * The component module is expected to have `vue` and `component` properties.
 *
 * @param {string} globalName - The name of the loaded component module.
 * @param {Object} data - Data to pass to the component on initialization
 * @param {Object} data.propsData - Initial props for the component
 * @returns A function that operates on a window, and resolves to original window,
 *          the vm, and element or the relevant component
 */
export function instantiateComponentFromLoadedGlobal(globalName: string, data: any): any {
  return (window: any) => {
    if (typeof window[globalName] === "undefined" || window[globalName] == null) {
      throw new Error("Incorrect/Invalid global name, failed to find window[" + globalName + "]");
    }

    // Grab the loaded element, component, and vue fn
    const vueFn  = window[globalName].default.vue;
    const comp  = vueFn.extend(window[globalName].default.component);

    // Create and mount component into off-page element
    const vm = (new comp(data)).$mount();
    const elem = vm.$el;

    // NOTE - the element is NOT on the page at this point, it"s off-page
    // The structure below is referred to as an ElemInfo object frequently.
    return {vm, elem, vueFn, comp, window};
  };
}

// Helper function to see if elem info is valid
function isValidElemInfoObj(obj: any): boolean {
  return obj.vm && obj.elem && obj.window && obj.comp;
}

/**
 * Add component"s element (expeted to be off-page) to the body of the HTML document.
 * Usually called after `instantiateComponentFromLoadedGlobal`
 *
 * @param {ElemInfo} info - Information about element/env see `instantiateComponentFromLoadedGlobal`
 * @returns A an object (elemInfo) that contains the window, the vm,
 * and element or the relevant component. see `instantiateComponentFromLoadedGlobal`
 */
export function addComponentElementToBody(info: any): any {
  if (!isValidElemInfoObj(info)) {
    throw new Error(`Invalid ElemInfo object: ${info}`);
  }

  // Add elem to document body
  info.window
    .document
    .documentElement
    .getElementsByTagName("body")[0]
    .appendChild(info.elem);

  // NOTE - the element is NOT on the page at this point, it"s off-page
  return info;
}

/**
 * Simulate a click in JSDOM
 *
 * @param {EventTarget} tgt - The element that should be the target for the click
 * @param {Window} window - The JSDOM-provided window of the page
 * @param {object} options - Click event options (by default an cancelable,
 * event that bubbles, with the given element as the view)
 * @param {Function} tgt.dispatchEvent - The element should be able to dispatch events.
 */
export function jsdomClick(tgt: EventTarget, window: any, options?: any): void {
  tgt.dispatchEvent(
    new window.MouseEvent("click", options || { bubbles: true, cancelable: true, view: tgt }),
  );
}

/**
 * Fail and end a test as provided by tape.
 *
 * @param {Object} t - Test to fail and end
 * @param {Function} t.fail - Should cause a failing assertion
 * @param {Function} t.fail - Should cause a failing assertion
 * @param {string} msg - Failure message
 * @returns A function that ignores it"s input, calls .fail then .end on the given t
 */
export function failAndEnd(t: Test, msg: string) {
  return (err: Error): void => {
    t.fail(msg || "failAndEnd called with no message");
    t.end(err);
  };
}

/**
 * Run some code in the context of a running app instance
 *
 * @param {Function} tape - `test` function provided by Tape
 * @param {string} name - The name of the child process that is going to be created
 * @param {Object} options - Options to use with the running app instance
 * @param {String|Function} options.cmd - Command to spawn in a child process
 * @param {Object} [options.process] - Object containing config to use for the spawned child process
 * @param {Object|Function} [options.process.env] - ENV variables that will be passed to the started process
 * @param {Function} fn - The function to run with the app instance. This function is a passed an object
 * with shape {info: {...}, process: ChildProcess }, with a possibly present error.
 * If the function returns a promise, the child process will be alive as long as the promise is alive.
 * @returns a Promise that resolves to an object containing information about the running child process
 */
function startChildProcess(t: Test, name: string, options: any): Promise<any> {
  return new Promise(resolve => {
    (options.port ? Promise.resolve(options.port) : getPort())
      .then((port: number) => {
        options.port = port;

        // Generate appropriate ENV to be passed to child process
        if (options.process &&
            options.process.env &&
            typeof options.process.env === "function") {
          options.process.env = options.process.env(options);
        }

        // Generate the data that will be passed back to the
        if (typeof options.info === "function") { options.info = options.info(options); }

        // Generate the command, if a function was provided
        if (typeof options.cmd === "function") { options.cmd = options.cmd(options); }

        // Spawn child process, keep a pointer to it to clean it up later
        const generatedProcess = childProcess.spawn(
          options.cmd.command,
          options.cmd.args,
          {env: options.process.env},
        );
        PROCESSES_TO_KILL.push(generatedProcess);

        // Log output of childprocess if specified
        if (options.logStdout) {
          generatedProcess.stdout.on("data", (d: string) => t.comment(`${name}.stdout: ${d}`));
        }
        if (options.logStderr) {
          generatedProcess.stderr.on("data", (d: string) => t.comment(`${name}.stderr: ${d}`));
        }

        t.comment(`Process ${name} started on port [${options.port}]`);

        setTimeout(
          () => resolve({
            info: options.info,
            process: generatedProcess,
            cleanup: () => generatedProcess.kill,
          }),
          options.afterStartDelayMs || 0,
        );

      });
  });
}

/**
 * Run the provided function with an app instance running in a child process.
 *
 * @param {Function} fn - The function to run (likely containing tests/assertions).
 * This function should expect an objects with shape {info: {...}, process: ChildProcess},
 * along with a possibly present error.
 */
export function startTestAppInstance(t: Test): Promise<any> {
  return startChildProcess(t, "App", {
    afterStartDelayMs: 500,
    process: {
      env: (options: any): any => {
        if (!process.env.DB_MIGRATION_FOLDER) { throw new Error("DB_MIGRATION_FOLDER ENV variable missing!"); }
        if (!process.env.FRONTEND_FOLDER) { throw new Error("FRONTEND_FOLDER ENV variable missing!"); }

        return {
          ENVIRONMENT: "Test",
          DB_MIGRATION_FOLDER: process.env.DB_MIGRATION_FOLDER,
          FRONTEND_FOLDER: process.env.FRONTEND_FOLDER,
          PORT: options.port,
        };
      },
    },

    info: (options: any) => {
      if (!process.env.TEST_APP_BIN_PATH) { throw new Error("TEST_APP_BIN_PATH ENV variable missing!"); }

      return Object.assign({
        binPath: process.env.TEST_APP_BIN_PATH,
        appUrl: `http://localhost:${options.port}`,
      }, options);
    },

    cmd: (options: any) => ({command: options.info.binPath, args: ["RunServer"]}),
  });
}

/**
 * Executes a function in a context in which a chromedriver browser session has been made
 *
 * @param {Function} tape - The tape (`test`) function
 * @param {Promise} chromeDriverInfoPromise - A promise that evaluates to information about the chromedriver
 * @param {Function} fn - The function to execute
 * @returns A promise that resolves to the result of the function execution with a browser passed into it
 */
export function startNewChromeDriverInstance(t: Test): Promise<any> {
  return new Promise(resolve => {
    return getPort()
      .then((port: number) => {

        const spawnedProcess = childProcess.spawn("chromedriver", [
          "--url-base=wd/hub",
          `--port=${port}`,
        ]);
        PROCESSES_TO_KILL.push(spawnedProcess);

        spawnedProcess.stdout.on("data", (d: string) => {
          if (`${d}`.match(/local connections are allowed/)) {
            t.comment(`Chromedriver expected to be running at localhost:${port}`);

            // Setup browser
            const browser = remote({port});

            // TODO: fail the test if the browser errors
            // browser.on("error", t.fail);

            resolve({browser, cleanup: () => t.comment("ending...")});
          }
        });
      });
  });
}

const ROUTE_FUNCTIONS: any = {
  // App endpoints
  login: (base: string) => `${base}/#/login`,
  app: (base: string) => `${base}/#/app`,
  admin: (base: string) => `${base}/#/app/admin`,
  manageCompanies: (base: string) => `${base}/#/app/admin/companies`,

  // API endpoints
  apiLogin: (base: string) => `${base}/api/v1/login`,
  apiUsers: (base: string) => `${base}/api/v1/users`,
  apiJobPostingRequests: (base: string) => `${base}/api/v1/jobs/postings/requests`,
  apiApproveJobPostingRequest: (base: string, rid: number) => `${base}/api/v1/jobs/postings/requests/${rid}/approve`,
  apiCompanies: (base: string) => `${base}/api/v1/companies`,
};

export function makeRouteURL(route: string): any { return ROUTE_FUNCTIONS[route]; }

/**
 * Perform login with the browser
 *
 * @param {tape.Tape} tape - tape instance
 * @param {any} browser - Browser
 * @param {string} email - email adddress
 * @param {string} password - password
 */
export function doLogin(
  t: Test,
  browser: any,
  appBaseURL: string,
  email: string,
  pass: string,
): Promise<void> {
  return browser
    .then(() => {
      return browser
        .url(makeRouteURL("login")(appBaseURL))
      // Ensure all the pieces exist
        .isExisting("input#email")
        .then((exists: boolean) => t.assert(exists, "email input rendered"))
        .then(() => browser.isExisting("input#password"))
        .then((exists: boolean) => t.assert(exists, "password input rendered"))
        .then(() => browser.isExisting("button#btn-login"))
        .then((exists: boolean) => t.assert(exists, "login button rendered"))
      // Input email
        .then(() => browser.setValue("input#email", email))
        .then(() => browser.getValue("input#email"))
        .then((expected: string) => t.equals(expected, email, "email should match"))
      // Input password
        .then(() => browser.setValue("input#password", pass))
        .then(() => browser.getValue("input#password"))
        .then((expected: string) => t.equals(expected, pass, "password should match"))
      // Click login button
        .click("button#btn-login")
        .pause(1000);
    });
}

// Utility function to logging in a given user
export function doLoginForUser(tape: Test, browser: any, appBaseURL: string, u: any) {
  return doLogin(tape, browser, appBaseURL, u.emailAddress, u.password);
}

/**
 * Do logins and get an auth cookie
 *
 * @param {Function} tape - The `test` function
 * @param {String} appBaseURL - base URL for the app
 * @param {String} email
 * @param {String} pass
 * @returns A promise that resolves to the auth cookie value
 */
export function doAPICookieLogin(tape: Test, appBaseURL: string, email: string, pass: string) {
  const requestOptions = {
    method: "POST",
    url: makeRouteURL("apiLogin")(appBaseURL),
    body: {userEmail: email, userPassword: pass},
    resolveWithFullResponse: true,
    json: true,
  };

  return rp(requestOptions)
    .then((resp: Response) => {
      if (resp.body.status !== "success") {
        throw new Error(`API Login to url [${requestOptions.url}] failed with message: [${resp.body.message}]`);
      }
      tape.ok(resp.body, "api login response body is valid");

      const results = /job-board-auth=([^;]+);/.exec(resp.headers["set-cookie"].join(" "));
      tape.assert(results.length > 0, "cookies were returned with the 'job-board-auth' name");

      return results[1];
    });
}

// Utility function for doing cookie-based login to the API for a given user
export function doAPICookieLoginForUser(tape: Test, appBaseURL: string, u: any) {
  return () => doAPICookieLogin(tape, appBaseURL, u.emailAddress, u.password);
}

/**
 * Create a backend entity by posting to a URL
 *
 * @param {Function} tape - `test` function provided by tape
 * @param {Object} opts - options
 * @param {string} [opts.authCookie] - Cookie value will be included in the request if provided,
 * with name "job-board-auth"
 * @param {string} opts.body - Request body
 * @returns a Promise that resolves to the server's response
 */
function makeEntity(tape: Test, url: string, opts: any) {
  const requestOptions: Options = {
    method: "POST",
    url,
    body: opts.body,
    json: true,
  };

  // Add cookie auth if provided
  if (opts.authCookie) {
    const j = rp.jar();
    j.setCookie(rp.cookie(`job-board-auth=${opts.authCookie}`), url);
    requestOptions.jar = j;
  }

  return rp(requestOptions)
    .then(({status, message, respData}: GenericResponseEnvelope) => {
      if (status !== "success") {
        throw new Error(`Entity creation using URL [${url}] failed with message: [${message}]`);
      }

      tape.ok(respData, "returned entity is valid");
      return respData;
    })
    .catch(tape.end);
}

// Helper for performing get requests
export function getReq(t: Test, url: string, opts: any) {
  const requestOptions: Options = {url, headers: {}, json: true};

  // Add cookie auth if provided
  if (opts.authCookie) {
    const j = rp.jar();
    j.setCookie(rp.cookie(`job-board-auth=${opts.authCookie}`), url);
    requestOptions.jar = j;
  }

  return rp(requestOptions)
    .then(({status, message, respData}: GenericResponseEnvelope) => {
      if (status !== "success") {
        throw new Error(`Entity creation using URL [${url}] failed with message: [${message}]`);
      }
      t.ok(respData, "returned entity is valid");
      return respData;
    })
    .catch(t.end);
}

// Utility function for making a User
export function makeUser(tape: Test, baseUrl: string) {
  return (opts: any) => makeEntity(tape, makeRouteURL("apiUsers")(baseUrl), opts);
}

// Utility function for retreiving users
export function getUsers(tape: Test, baseUrl: string, opts: any) {
  return getReq(tape, makeRouteURL("apiUsers")(baseUrl), opts);
}

// Helper to make companies
export function makeCompany(tape: Test, baseUrl: string, opts: any) {
  return makeEntity(tape, makeRouteURL("apiCompanies")(baseUrl), opts);
}

// Helper to make job posting requests
export function makeJobPostingRequest(tape: Test, baseUrl: string, opts: any) {
  return makeEntity(tape, makeRouteURL("apiJobPostingRequests")(baseUrl), opts);
}

// Helper to make job posting requests
export function approveJobPostingRequest(tape: Test, baseUrl: string, jprId: string, opts: any) {
  return getReq(tape, makeRouteURL("apiApproveJobPostingRequest")(baseUrl, jprId), opts);
}

// Set up chrome driver
export function chromeDriverSetupTest(tape?: any) {
  let promiseResolver: any;
  const chromeDriverPromise = new Promise((resolve) => promiseResolver = resolve);

  test("set up chromedriver", defaultTestConfig, (t: Test) => {
    startNewChromeDriverInstance(t)
      .then(promiseResolver)
      .then(() => t.comment("chrome driver started"))
      .then(() => t.end(), t.end);
  });

  return chromeDriverPromise;
}

export function chromeDriverCleanupTest(
  tape: Test,
  browserPromise: Promise<BrowserObject>,
) {
  return test("teardown chromedriver", defaultTestConfig, (t: Test) => {
    browserPromise
      .then((b: BrowserObject) => b.shutdown())
      .then(() => t.end(), t.end);
  });
}
