// Default error (when absolutely no options are specified to the component instance)
export const DEFAULT_ERROR: any = {
  propsData: {
    alert: {
      closable: true,
      status: undefined,
      message: undefined,
    },
    alertIdx: undefined,
  },
};

// Fixture for a simple alert @ init (prop)
export const CLOSABLE_ERROR: any = {
  propsData: {
    alert: {
      closable: true,
      status: "error",
      message: "This is a test alert",
    },
    alertIdx: 0,
  },
};

// Create a single hint
export const HINT_CREATION: any = {
  propsData: {
    hintCreateFn: (h: any) => Promise.resolve(h),
  },
};
