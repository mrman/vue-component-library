"use strict";
exports.__esModule = true;
exports.DEFAULT_ERROR = {
    propsData: {
        alert: {
            closable: true,
            status: undefined,
            message: undefined
        },
        alertIdx: undefined
    }
};
exports.CLOSABLE_ERROR = {
    propsData: {
        alert: {
            closable: true,
            status: "error",
            message: "This is a test alert"
        },
        alertIdx: 0
    }
};
exports.HINT_CREATION = {
    propsData: {
        hintCreateFn: function (h) { return Promise.resolve(h); }
    }
};
//# sourceMappingURL=alert-notification.js.map