"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var vue_1 = __importDefault(require("vue"));
var get_port_1 = __importDefault(require("get-port"));
var process_1 = __importDefault(require("process"));
var request_promise_native_1 = __importDefault(require("request-promise-native"));
var child_process_1 = __importDefault(require("child_process"));
var vue_server_renderer_1 = require("vue-server-renderer");
var jsdom_1 = require("jsdom");
var webdriverio_1 = require("webdriverio");
var tape_1 = __importDefault(require("tape"));
var renderer = vue_server_renderer_1.createRenderer();
var PROCESSES_TO_KILL = [];
var WAIT_BEFORE_KILL_AND_EXIT_MS = 500;
process_1["default"].on("SIGINT", function () { return PROCESSES_TO_KILL.forEach(function (p) { return p.kill(); }); });
process_1["default"].on("exit", function () { return PROCESSES_TO_KILL.forEach(function (p) { return p.kill(); }); });
tape_1["default"].onFinish(function () {
    setTimeout(function () {
        PROCESSES_TO_KILL.forEach(function (p) { return p.kill(); });
    }, WAIT_BEFORE_KILL_AND_EXIT_MS);
});
exports.defaultTestConfig = { timeout: 10000 };
function renderComponentWithData(Component, props) {
    var ModifiedVue = vue_1["default"].extend(Component);
    var vm = new ModifiedVue(props || {});
    return new Promise(function (resolve, reject) {
        renderer.renderToString(vm, function (err, html) {
            if (err) {
                reject(err);
                return;
            }
            resolve(new jsdom_1.JSDOM(html).window);
        });
    });
}
exports.renderComponentWithData = renderComponentWithData;
function renderModuleInJSDOM(path, name) {
    return Promise.resolve(new jsdom_1.JSDOM("<script name=" + name + " src=" + path + "></script>", { runScripts: "dangerously" }).window);
}
exports.renderModuleInJSDOM = renderModuleInJSDOM;
function instantiateComponentFromLoadedGlobal(globalName, data) {
    return function (window) {
        if (typeof window[globalName] === "undefined" || window[globalName] == null) {
            throw new Error("Incorrect/Invalid global name, failed to find window[" + globalName + "]");
        }
        var vueFn = window[globalName]["default"].vue;
        var comp = vueFn.extend(window[globalName]["default"].component);
        var vm = (new comp(data)).$mount();
        var elem = vm.$el;
        return { vm: vm, elem: elem, vueFn: vueFn, comp: comp, window: window };
    };
}
exports.instantiateComponentFromLoadedGlobal = instantiateComponentFromLoadedGlobal;
function isValidElemInfoObj(obj) {
    return obj.vm && obj.elem && obj.window && obj.comp;
}
function addComponentElementToBody(info) {
    if (!isValidElemInfoObj(info)) {
        throw new Error("Invalid ElemInfo object: " + info);
    }
    info.window
        .document
        .documentElement
        .getElementsByTagName("body")[0]
        .appendChild(info.elem);
    return info;
}
exports.addComponentElementToBody = addComponentElementToBody;
function jsdomClick(tgt, window, options) {
    tgt.dispatchEvent(new window.MouseEvent("click", options || { bubbles: true, cancelable: true, view: tgt }));
}
exports.jsdomClick = jsdomClick;
function failAndEnd(t, msg) {
    return function (err) {
        t.fail(msg || "failAndEnd called with no message");
        t.end(err);
    };
}
exports.failAndEnd = failAndEnd;
function startChildProcess(t, name, options) {
    return new Promise(function (resolve) {
        (options.port ? Promise.resolve(options.port) : get_port_1["default"]())
            .then(function (port) {
            options.port = port;
            if (options.process &&
                options.process.env &&
                typeof options.process.env === "function") {
                options.process.env = options.process.env(options);
            }
            if (typeof options.info === "function") {
                options.info = options.info(options);
            }
            if (typeof options.cmd === "function") {
                options.cmd = options.cmd(options);
            }
            var generatedProcess = child_process_1["default"].spawn(options.cmd.command, options.cmd.args, { env: options.process.env });
            PROCESSES_TO_KILL.push(generatedProcess);
            if (options.logStdout) {
                generatedProcess.stdout.on("data", function (d) { return t.comment(name + ".stdout: " + d); });
            }
            if (options.logStderr) {
                generatedProcess.stderr.on("data", function (d) { return t.comment(name + ".stderr: " + d); });
            }
            t.comment("Process " + name + " started on port [" + options.port + "]");
            setTimeout(function () { return resolve({
                info: options.info,
                process: generatedProcess,
                cleanup: function () { return generatedProcess.kill; }
            }); }, options.afterStartDelayMs || 0);
        });
    });
}
function startTestAppInstance(t) {
    return startChildProcess(t, "App", {
        afterStartDelayMs: 500,
        process: {
            env: function (options) {
                if (!process_1["default"].env.DB_MIGRATION_FOLDER) {
                    throw new Error("DB_MIGRATION_FOLDER ENV variable missing!");
                }
                if (!process_1["default"].env.FRONTEND_FOLDER) {
                    throw new Error("FRONTEND_FOLDER ENV variable missing!");
                }
                return {
                    ENVIRONMENT: "Test",
                    DB_MIGRATION_FOLDER: process_1["default"].env.DB_MIGRATION_FOLDER,
                    FRONTEND_FOLDER: process_1["default"].env.FRONTEND_FOLDER,
                    PORT: options.port
                };
            }
        },
        info: function (options) {
            if (!process_1["default"].env.TEST_APP_BIN_PATH) {
                throw new Error("TEST_APP_BIN_PATH ENV variable missing!");
            }
            return Object.assign({
                binPath: process_1["default"].env.TEST_APP_BIN_PATH,
                appUrl: "http://localhost:" + options.port
            }, options);
        },
        cmd: function (options) { return ({ command: options.info.binPath, args: ["RunServer"] }); }
    });
}
exports.startTestAppInstance = startTestAppInstance;
function startNewChromeDriverInstance(t) {
    return new Promise(function (resolve) {
        return get_port_1["default"]()
            .then(function (port) {
            var spawnedProcess = child_process_1["default"].spawn("chromedriver", [
                "--url-base=wd/hub",
                "--port=" + port,
            ]);
            PROCESSES_TO_KILL.push(spawnedProcess);
            spawnedProcess.stdout.on("data", function (d) {
                if (("" + d).match(/local connections are allowed/)) {
                    t.comment("Chromedriver expected to be running at localhost:" + port);
                    var browser_1 = webdriverio_1.remote({ port: port });
                    resolve({ browser: browser_1, cleanup: function () { return t.comment("ending..."); } });
                }
            });
        });
    });
}
exports.startNewChromeDriverInstance = startNewChromeDriverInstance;
var ROUTE_FUNCTIONS = {
    login: function (base) { return base + "/#/login"; },
    app: function (base) { return base + "/#/app"; },
    admin: function (base) { return base + "/#/app/admin"; },
    manageCompanies: function (base) { return base + "/#/app/admin/companies"; },
    apiLogin: function (base) { return base + "/api/v1/login"; },
    apiUsers: function (base) { return base + "/api/v1/users"; },
    apiJobPostingRequests: function (base) { return base + "/api/v1/jobs/postings/requests"; },
    apiApproveJobPostingRequest: function (base, rid) { return base + "/api/v1/jobs/postings/requests/" + rid + "/approve"; },
    apiCompanies: function (base) { return base + "/api/v1/companies"; }
};
function makeRouteURL(route) { return ROUTE_FUNCTIONS[route]; }
exports.makeRouteURL = makeRouteURL;
function doLogin(t, browser, appBaseURL, email, pass) {
    return browser
        .then(function () {
        return browser
            .url(makeRouteURL("login")(appBaseURL))
            .isExisting("input#email")
            .then(function (exists) { return t.assert(exists, "email input rendered"); })
            .then(function () { return browser.isExisting("input#password"); })
            .then(function (exists) { return t.assert(exists, "password input rendered"); })
            .then(function () { return browser.isExisting("button#btn-login"); })
            .then(function (exists) { return t.assert(exists, "login button rendered"); })
            .then(function () { return browser.setValue("input#email", email); })
            .then(function () { return browser.getValue("input#email"); })
            .then(function (expected) { return t.equals(expected, email, "email should match"); })
            .then(function () { return browser.setValue("input#password", pass); })
            .then(function () { return browser.getValue("input#password"); })
            .then(function (expected) { return t.equals(expected, pass, "password should match"); })
            .click("button#btn-login")
            .pause(1000);
    });
}
exports.doLogin = doLogin;
function doLoginForUser(tape, browser, appBaseURL, u) {
    return doLogin(tape, browser, appBaseURL, u.emailAddress, u.password);
}
exports.doLoginForUser = doLoginForUser;
function doAPICookieLogin(tape, appBaseURL, email, pass) {
    var requestOptions = {
        method: "POST",
        url: makeRouteURL("apiLogin")(appBaseURL),
        body: { userEmail: email, userPassword: pass },
        resolveWithFullResponse: true,
        json: true
    };
    return request_promise_native_1["default"](requestOptions)
        .then(function (resp) {
        if (resp.body.status !== "success") {
            throw new Error("API Login to url [" + requestOptions.url + "] failed with message: [" + resp.body.message + "]");
        }
        tape.ok(resp.body, "api login response body is valid");
        var results = /job-board-auth=([^;]+);/.exec(resp.headers["set-cookie"].join(" "));
        tape.assert(results.length > 0, "cookies were returned with the 'job-board-auth' name");
        return results[1];
    });
}
exports.doAPICookieLogin = doAPICookieLogin;
function doAPICookieLoginForUser(tape, appBaseURL, u) {
    return function () { return doAPICookieLogin(tape, appBaseURL, u.emailAddress, u.password); };
}
exports.doAPICookieLoginForUser = doAPICookieLoginForUser;
function makeEntity(tape, url, opts) {
    var requestOptions = {
        method: "POST",
        url: url,
        body: opts.body,
        json: true
    };
    if (opts.authCookie) {
        var j = request_promise_native_1["default"].jar();
        j.setCookie(request_promise_native_1["default"].cookie("job-board-auth=" + opts.authCookie), url);
        requestOptions.jar = j;
    }
    return request_promise_native_1["default"](requestOptions)
        .then(function (_a) {
        var status = _a.status, message = _a.message, respData = _a.respData;
        if (status !== "success") {
            throw new Error("Entity creation using URL [" + url + "] failed with message: [" + message + "]");
        }
        tape.ok(respData, "returned entity is valid");
        return respData;
    })["catch"](tape.end);
}
function getReq(t, url, opts) {
    var requestOptions = { url: url, headers: {}, json: true };
    if (opts.authCookie) {
        var j = request_promise_native_1["default"].jar();
        j.setCookie(request_promise_native_1["default"].cookie("job-board-auth=" + opts.authCookie), url);
        requestOptions.jar = j;
    }
    return request_promise_native_1["default"](requestOptions)
        .then(function (_a) {
        var status = _a.status, message = _a.message, respData = _a.respData;
        if (status !== "success") {
            throw new Error("Entity creation using URL [" + url + "] failed with message: [" + message + "]");
        }
        t.ok(respData, "returned entity is valid");
        return respData;
    })["catch"](t.end);
}
exports.getReq = getReq;
function makeUser(tape, baseUrl) {
    return function (opts) { return makeEntity(tape, makeRouteURL("apiUsers")(baseUrl), opts); };
}
exports.makeUser = makeUser;
function getUsers(tape, baseUrl, opts) {
    return getReq(tape, makeRouteURL("apiUsers")(baseUrl), opts);
}
exports.getUsers = getUsers;
function makeCompany(tape, baseUrl, opts) {
    return makeEntity(tape, makeRouteURL("apiCompanies")(baseUrl), opts);
}
exports.makeCompany = makeCompany;
function makeJobPostingRequest(tape, baseUrl, opts) {
    return makeEntity(tape, makeRouteURL("apiJobPostingRequests")(baseUrl), opts);
}
exports.makeJobPostingRequest = makeJobPostingRequest;
function approveJobPostingRequest(tape, baseUrl, jprId, opts) {
    return getReq(tape, makeRouteURL("apiApproveJobPostingRequest")(baseUrl, jprId), opts);
}
exports.approveJobPostingRequest = approveJobPostingRequest;
function chromeDriverSetupTest(tape) {
    var promiseResolver;
    var chromeDriverPromise = new Promise(function (resolve) { return promiseResolver = resolve; });
    tape_1["default"]("set up chromedriver", exports.defaultTestConfig, function (t) {
        startNewChromeDriverInstance(t)
            .then(promiseResolver)
            .then(function () { return t.comment("chrome driver started"); })
            .then(function () { return t.end(); }, t.end);
    });
    return chromeDriverPromise;
}
exports.chromeDriverSetupTest = chromeDriverSetupTest;
function chromeDriverCleanupTest(tape, browserPromise) {
    return tape_1["default"]("teardown chromedriver", exports.defaultTestConfig, function (t) {
        browserPromise
            .then(function (b) { return b.shutdown(); })
            .then(function () { return t.end(); }, t.end);
    });
}
exports.chromeDriverCleanupTest = chromeDriverCleanupTest;
//# sourceMappingURL=util.js.map