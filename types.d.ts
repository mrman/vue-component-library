export interface User {
    emailAddress: string;
    password: string;
}
export declare enum ResponseStatus {
    success = "success",
    error = "error"
}
export interface GenericResponseEnvelope {
    status: ResponseStatus;
    message: string;
    respData: any;
}
export interface Address {
    id?: number;
    line1: string;
    line2: string;
    zipCode: string;
    town: string;
    state: string;
    country: string;
}
export interface Company {
    id: number;
    name: string;
    description: string;
    cultureDescription: string;
    homepageUrl: string;
    iconUri: null;
    bannerImageUri: string;
    addressId: number;
    primaryColorCSS: string;
    createdAt: Date;
    lastUpdatedAt: Date;
}
export interface Tag {
    name: string;
    value?: string;
    cssColor: string;
    names: object;
    createdAt: Date;
}
export interface Job {
    id: number;
    title: string;
    description: string;
    industry: string;
    type: string;
    minSalary: number;
    maxSalary: number;
    salaryCurrency: string;
    postingDate: Date;
    employerId: number;
    isActive: boolean;
    tags: Tag[];
    applyLink: string;
    urlBounceConfigId: number;
}
export interface Stringable {
    toString: () => string;
}
export interface SearchParams {
    limit: number;
    offset: number;
    pageSize: number;
}
export interface PaginationPreferences {
    listStyle: string;
    currentPage: number;
    offset: number;
}
export interface Option {
    label: string;
    value: any;
}
