import AlertListing from "./components/alert-listing/component.vue";
import AlertNotification from "./components/alert-notification/component.vue";
import AsyncButton from "./components/async-button/component.vue";
import CustomOptionSelect from "./components/custom-option-select/component.vue";
import DashboardTextWidget from "./components/dashboard-text-widget/component.vue";
import GenericListing from "./components/generic-listing/component.vue";
import ListingTableItem from "./components/listing-table-item/component.vue";
import LoadingIndicator from "./components/loading-indicator/component.vue";
import PanelContainer from "./components/panel-container/component.vue";
import PasswordChangeForm from "./components/password-change-form/component.vue";
import PrintedAddress from "./components/printed-address/component.vue";
import TagListingItem from "./components/tag-listing-item/component.vue";
import TagToggleSelector from "./components/tag-toggle-selector/component.vue";
import VuePictureInput from "./components/vendor/vue-picture-input/component.vue";

export default {
  install: (Vue) =>  {
    Vue.component("alert-listing", AlertListing);
    Vue.component("alert-notification", AlertNotification);
    Vue.component("async-button", AsyncButton);
    Vue.component("custom-option-select", CustomOptionSelect);
    Vue.component("dashboard-text-widget", DashboardTextWidget);
    Vue.component("generic-listing", GenericListing);
    Vue.component("listing-table-item", ListingTableItem)
    Vue.component("loading-indicator", LoadingIndicator);
    Vue.component("panel-container", PanelContainer);
    Vue.component("password-change-form", PasswordChangeForm);
    Vue.component("printed-address", PrintedAddress);
    Vue.component("tag-listing-item", TagListingItem);
    Vue.component("tag-toggle-selector", TagToggleSelector);
    Vue.component("vue-picture-input", VuePictureInput);
  }
}
