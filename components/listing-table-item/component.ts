import prettyEnum from "../../filters/pretty-enum";

const DEFAULT_DISPLAY_CFG = {
  formatter: (i: any): any => i
};

export default {
  filters: {
    prettyEnum,
  },

  props: {
    displayCfg: {type: Object, default: () => DEFAULT_DISPLAY_CFG },
    data: Object,
    dataIdx: Number
  },

  data: function() {
    // Fill in displayCfg if it's missing stuff
    return {
      dcfg: Object.assign({}, DEFAULT_DISPLAY_CFG, this.displayCfg)
    };
  },

  computed: {
    value: function() {
      if (!this.displayCfg.field) {
        return this.data;
      }

      // Return the value if there's an exact match
      if (this.displayCfg.field in this.data) {
        return this.data[this.displayCfg.field];
      }

      // Recur into the structure (if necessary)
      return this.displayCfg.field
        .split(".")
        .reduce((acc, key) => acc[key], this.data);
    }
  }
};
