declare const _default: {
    props: {
        alert: ObjectConstructor;
        alertIdx: NumberConstructor;
        closable: {
            type: BooleanConstructor;
            default: boolean;
        };
        dismissAfterMs: NumberConstructor;
        status: StringConstructor;
        message: StringConstructor;
    };
    created: () => void;
    computed: {
        isClosable: () => any;
        alertMessage: () => any;
        alertStatus: () => any;
        alertClass: () => string;
    };
    methods: {
        destroy: () => void;
    };
};
export default _default;
