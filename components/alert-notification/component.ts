export default {
  props: {
    alert: Object,
    alertIdx: Number,
    closable: {type: Boolean, default: true},
    dismissAfterMs: Number,

    // For alerts that have the properties specified directly
    status: String,
    message: String
  },

  created: function() {
    var dismissAfterMs = this.dismissAfterMs || this.alert ? this.alert.dismissAfterMs : null;
    if (dismissAfterMs) { setTimeout(this.destroy, dismissAfterMs); }
  },

  computed: {
    isClosable: function() { return this.alert ? this.alert.closable : this.closable; },
    alertMessage: function() { return this.alert ? this.alert.message : this.message; },
    alertStatus: function() { return this.alert ? this.alert.status : this.status; },

    alertClass: function() {
      return [
        "alert",
        this.alertStatus ? `alert-${this.alertStatus}` : ""
      ].join(" ");
    }
  },

  methods: {
    // Destroy this alert (emits event to parent)
    destroy: function() {
      if (this.alert) {
        this.$emit("destroy", this.alert, this.alertIdx);
      } else {
        // If alert notification was used with status/message passed in directly,
        // need to create an alert object to report has been destroyed.
        this.$emit(
          "destroy",
          {
            status: this.status,
            message: this.message,
            closable: this.closable
          },
          this.alertIdx
        );
      }
    }
  }
};
