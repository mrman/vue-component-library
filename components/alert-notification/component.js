"use strict";
exports.__esModule = true;
exports["default"] = {
    props: {
        alert: Object,
        alertIdx: Number,
        closable: { type: Boolean, "default": true },
        dismissAfterMs: Number,
        status: String,
        message: String
    },
    created: function () {
        var dismissAfterMs = this.dismissAfterMs || this.alert ? this.alert.dismissAfterMs : null;
        if (dismissAfterMs) {
            setTimeout(this.destroy, dismissAfterMs);
        }
    },
    computed: {
        isClosable: function () { return this.alert ? this.alert.closable : this.closable; },
        alertMessage: function () { return this.alert ? this.alert.message : this.message; },
        alertStatus: function () { return this.alert ? this.alert.status : this.status; },
        alertClass: function () {
            return [
                "alert",
                this.alertStatus ? "alert-" + this.alertStatus : ""
            ].join(" ");
        }
    },
    methods: {
        destroy: function () {
            if (this.alert) {
                this.$emit("destroy", this.alert, this.alertIdx);
            }
            else {
                this.$emit("destroy", {
                    status: this.status,
                    message: this.message,
                    closable: this.closable
                }, this.alertIdx);
            }
        }
    }
};
//# sourceMappingURL=component.js.map