import TagListingItem from "../tag-listing-item/component.vue";
import { Tag } from "../../types"

export default {
  props: {
    // Tags are expected to look like: {name: x, value: 1}
    tags: Array,
    initialSelectedTagValues: Array
  },

  components: {
    TagListingItem,
  },

  data: function() {
    return {
      selectedTagValues: this.initialSelectedTagValues || []
    };
  },

  methods: {
    toggleTag: function(tag: Tag) {
      if (this.selectedTagValues.includes(tag.value)) {
        this.selectedTagValues = this.selectedTagValues.filter((t: string) => t === tag.value);
      } else {
        this.selectedTagValues.push(tag.value);
      }

      this.$emit("selected-tag-values-changed", this.selectedTagValues);
    }
  }
};
