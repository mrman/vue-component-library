export default {
  props: {
    title: String,
    subtitle: String,
    hoverDesc: String,
    value: Number,
    iconClass: String
  }
};
