import AlertNotification from "../alert-notification/component.vue";

export default {
  props: {
    alerts: Array,
    maxAlertCount: {
      type: Number,
      default: -1
    }
  },

  components: {
    AlertNotification,
  },

  computed: {
    processedAlerts: function() {
      // If maxAlertCount is specified, remove all others (till max is reached)
      // This assumes that new entries are PUSHED.
      while (this.maxAlertCount != -1 && this.alerts.length > this.maxAlertCount) {
        this.alerts.splice(0, 1);
      }

      return this.alerts;
    }
  },

  methods: {
    // Remove an alert
    destroyAlert: function(alert: any, idx: number) {
      var a = this.alerts[idx];

      if (a.status === alert.status &&
          a.message === alert.message) {
        this.alerts.splice(idx, 1);
      }

    }
  }
};
