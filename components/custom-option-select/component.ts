import { Option } from "../../types";

import prettyEnum from "../../filters/pretty-enum";

export default {
  props: {
    prettyEnumTranslations: String,
    description: String,
    rawValues: {type: Array, default: null}, // ['one', 'two']
    options: {type: Array, default: null}, // [ {label: '...', value: 'one'}, ...]
    selectedValue: [String, Number]
  },

  filters: {
    prettyEnum,
  },

  data: function() {
    if (this.rawValues && this.options || (!this.rawValues && !this.options)) {
      throw new Error("Either `raw-values` or `options` should be specified. Only one should be used");
    }

    return {
      value: this.selectedValue || "",
      values: this.rawValues
    };
  },

  computed: {
    selectOptions: function() {
      if (this.options) { return this.options; }

      return this.values.map((v: any) => ({
        value: v,
        label: this.$options.filters.prettyEnum(v, this.prettyEnumTranslations)
      }));
    }
  },

  watch: {
    // Update props if they get updated in the parent
    selectedValue: function(v: string | number) { this.value = v; },
    rawValues: function(vs: string[]) { this.values = vs; },
    options: function(os: Option[]) { this.options = os; },

    value: function(option: string | number) { this.$emit("value-changed", {value: option}); }
  }

};
