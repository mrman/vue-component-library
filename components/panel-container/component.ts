export default {
  props: {
    lhsCompressed: { type: Boolean, default: false },
    lhsMinWidth: { type: String, default: "0" },
    lhsAdditionalClasses: { type: String, default: "" }
  },

  computed: {

    processedLHSClasses: function() {
      return this.lhsAdditionalClasses
        .split(" ")
        .concat([ this.lhsCompressed ? "compressed" : "" ])
        .join(" ");
    },

    lhsStyle: function() {
      return {
        width: this.lhsCompressed ? this.lhsMinWidth : undefined
      };
    }

  }
};
