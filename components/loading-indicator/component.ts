export default {
  props: {
    headerImageSrc: String,
    isLoading: Boolean,
    title: String,
    description: String
  },

  computed: {
    additionalClasses: function() {
      return this.isLoading ? "" : "dismissed";
    }
  }
};
