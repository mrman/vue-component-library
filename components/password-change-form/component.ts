import AlertNotification from "../alert-notification/component.vue";
import AlertListing from "../alert-listing/component.vue";

export default {
  props: {
    alertSvc: {type: Object, required: true},
    cmdBus: {type: Object }
  },

  components: {
    AlertNotification,
    AlertListing,
  },

  data: function() {

    if (this.cmdBus) {
      this.cmdBus.$on("reset", this.resetForm);
      this.cmdBus.$on("validate-and-submit", this.validateAndSubmit);

      this.cmdBus.$on("submission-failed", (msg: string) => {
        if (this.alertSvc) {
          this.alertSvc.addAlertForNamespace({
            status: "error",
            message: msg
          }, "password-change-form");
        }
      });
    }

    return {
      submittedOnceWithErrors: false,
      submissionSucceeded: false,
      submissionErrored: false,

      oldPassword: "",
      newPassword: "",
      newPasswordAgain: "",
      showPlainTextPassword: false,

      alerts: this.alertSvc.getAlertsForNamespace("password-change-form")
    };
  },

  computed: {
    oldPasswordIsValid: function() { return this.oldPassword.trim().length > 0; },
    newPasswordIsValid: function() { return this.newPassword.trim().length > 0; },
    newPasswordAgainIsValid: function() {
      return this.newPassword === this.newPasswordAgain
        && this.newPasswordAgain.trim().length > 0;
    },

    hasErrors: function() {
      return [
        this.oldPasswordIsValid,
        this.newPasswordIsValid,
        this.newPasswordAgainIsValid
      ].some(v => !v);
    }
  },

  methods: {
    resetForm: function() {
      this.oldPassword = "";
      this.newPassword = "";
      this.newPasswordAgain = "";
      this.submittedOnceWithErrors = false;
    },

    validateAndSubmit: function() {
      if (this.hasErrors) {
        this.submittedOnceWithErrors = true;
        return;
      }

      this.$emit("submit", {
        oldPassword: this.oldPassword,
        newPassword: this.newPassword,
        newPasswordAgain: this.newPasswordAgain
      });
    }
  }

};
