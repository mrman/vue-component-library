export default {
  props: {
    tag: Object,
    iconImageSrc: String,
    selected: Boolean,
    tagIdx: Number
  },

  computed: {
    tagClass: function() {
      var selectedCls = this.selected ? "selected" : "";
      return ["tag-listing-item-component", "sm-margin-right", "cursor-pointer", selectedCls].join(" ");
    }
  }
};
