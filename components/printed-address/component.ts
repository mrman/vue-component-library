import { isNullOrUndefined } from "../../util";
import { Address } from "../../types";

export default {
  props: {
    addressesSvc: Object,

    addressId: Number,
    data: Object
  },

  data: function() {
    if (isNullOrUndefined(this.data) && isNullOrUndefined(this.addressId)) {
      throw new Error("Must provide address or an ID to fetch");
    }

    // If an address ID was given but no data was provided then fetch
    if (!isNullOrUndefined(this.addressId) && isNullOrUndefined(this.data)) {
      this.fetchAddress();
    }

    return { address: this.data || null };
  },

  methods: {
    fetchAddress: function() {
      if (!this.addressesSvc) { return Promise.reject(); }

      return this.addressesSvc
        .getOrFetchAddressById(this.addressId)
        .then((a: Address) => this.address = a );
    }
  }
};
