import { isNullOrUndefined } from "../../util";
import { SearchParams, PaginationPreferences } from "../../types";

import ListingTableItem from "../listing-table-item/component.vue";

const VALID_LIST_STYLES = ["list", "grid-small", "grid-large"];
const DEFAULT_PAGE_SIZE_OPTIONS = [10, 25, 50];
const FETCH_THROTTLE_INTERVAL_MS = 3000;

export default {
  components: {
    ListingTableItem,
  },

  props: {
    scrollSvc: Object,
    prefSvc: Object,
    prefKey: String,

    type: {type: String, required: true}, // see VALID_TYPES

    cmdBus: Object, // Used to trigger searches/other functionality from parent

    // Either `url` (server-side) or `data` (client-side) will be provided, not both
    url: String,
    getParamTransformFn: Function,

    // Function for transforming the URL
    urlTransformFn: Function,
    // Function that takes a URL (fully formed with search params, returns a promise that evaluates to an object like {items: [...], count: ...}
    reqFn: Function,

    initialData: Array,
    apiConfig: Object, // should only be specified if getData was specified

    // If `columns` is not specified, it will be treated as a string and printed
    columnDisplayCfg: {type: Array},

    rowKeyField: {type: String, required: true},

    modelClass: {type: Function},

    filterable: Boolean,
    filterFields: Array,
    filterTerm: String,
    throttleInterval: {type: Number, default: FETCH_THROTTLE_INTERVAL_MS},

    sortable: Boolean, // TODO: Implement (move to a property of column display configs)

    paginated: Boolean,
    pageSizeOptions: {type: Array, default: () => DEFAULT_PAGE_SIZE_OPTIONS}, // ex. [10, 15, 25]

    endlessScroll: Boolean,

    showActionColumn: Boolean,
    multiSelectable: Boolean,  // TODO: Implement

    enableListStyleChange: {type: Boolean, default: false},
    initialListStyle: {type: String, default: "list"},

    initialPageNumber: {type: Number, default: 1},
    initialOffset: {type: Number, default: 0}
  },

  data: function() {
    // Sanity checks
    if (this.type === "table" && !this.columnDisplayCfg) { throw new Error("columnDisplayCfg is required, for 'table' type listings"); }
    if (!isNullOrUndefined(this.url) && !isNullOrUndefined(this.initialData)) { throw new Error("Both `url` and `initialData` cannot be specified"); }
    if (this.filterable && isNullOrUndefined(this.filterFields)) { throw new Error("Fields on which to filter must be specified if filtering is allowed"); }
    if (this.paginated && isNullOrUndefined(this.pageSizeOptions)) { throw new Error("`pageSizeOptions` should be provided (or the default used) for paginated listings"); }
    if (this.paginated && this.endlessScroll) { throw new Error("Both `paginated` and `endlessScroll` cannot be specified"); }

    var fetch = this.fetchListingData.bind(this);

    if (this.cmdBus) {
      this.cmdBus.$on("do-search", () => { fetch(); });
    }

    // Load preferences if a prefKey is provided
    if (this.prefKey && this.prefSvc) {
      this.prefSvc
        .loadPreferences(this.prefKey)
        .then(this.applyPreferences);
    }

    return {
      filterText: this.filterTerm || "",
      listStyle: this.initialListStyle,

      currentPageNumber: this.initialPageNumber,
      offset: this.initialOffset,
      pageSize: this.pageSizeOptions[0],

      totalCount: null as number,

      endlessScrollListBottom: false,

      scrollWatchUnbindFn: () => {},

      data: this.initialData,
      requestInFlight: false,
      fetchFn: fetch
    };
  },

  created: function() {
    // If data must be fetched, kick it off
    if (this.url || this.reqFn) {
      const params = this.generateQueryParams({offset: this.initialOffset, pageSize: this.pageSizeOptions[0]});
      this.fetchListingData(params);
    }
  },

  mounted: function() {
    if (this.scrollSvc && this.endlessScroll) {
      this.scrollSvc.$on("scroll", () => {
        var fullyScrolled = this.$el.scrollHeight - this.$el.scrollTop / this.$el.clientHeight * 1.0;

        // If there are still things to fetch, fetch
        if (fullyScrolled && this.data && this.data.length < this.totalCount) {
          this.fetchListingData(this.generateQueryParams());
        }

      });
    }
  },

  computed: {
    isServerSide: function() { return !isNullOrUndefined(this.url || this.reqFn); },

    isDynamicListing: function() { return this.type === "dynamic"; },

    listStyleGridLarge: function() { return this.listStyle === "grid-large"; },
    listStyleGridSmall: function() { return this.listStyle === "grid-small"; },
    listStyleList: function() { return this.listStyle === "list"; },

    totalColumns: function() {
      return this.columnDisplayCfg.length + (this.showActionColumn ? 1 : 0);
    },

    maxPageNumber: function() {
      if (isNullOrUndefined(this.data)) {
        return 0;
      }

      return Math.ceil((this.isServerSide ? this.totalCount : this.data.length) / (this.pageSize * 1.0));
    },

    processedData: function() {
      if (isNullOrUndefined(this.data)) { return []; }

      var processed = this.data;

      // Server data has offset taken care of
      if (!this.isServerSide) {
        var offset = this.offset * this.pageSize;
        processed = processed.slice(offset, offset + this.pageSize);
      }

      // Get the data, with listing IDs to feed to :key
      processed = processed.map((d: any) => Object.assign({}, d, {_listingId: d[this.rowKeyField]}));

      // Filter by `filterText`, if filtering is on and present
      if (this.filterable && this.filterText) {
        const matchesFilterText = (ftxt: any) => (d: any) => (f: any) => {
          const value = d[f];
          const lowercasedFilterText = ftxt.toLocaleLowerCase();
          const lowercasedValue = value.toString().toLocaleLowerCase();
          return !isNullOrUndefined(value) && lowercasedValue.includes(lowercasedFilterText);
        };
        processed = processed.filter((d: any) => this.filterFields.some(matchesFilterText(this.filterText)(d)));
      }

      return processed;
    }
  },

  watch: {
    initialData: function() {
      if (!this.isServerSide) {
        this.data = this.initialData;
      }
    },

    pageSize: function() {
      // If the server is backend paginated, need to re-fetch (where maxPageNumber will be set)
      if (this.isServerSide) {
        this.fetchListingData(this.generateQueryParams());
      }
    }

  },

  methods: {
    // Apply preferences that might be present
    applyPreferences: function(prefs: PaginationPreferences) {
      if (prefs.listStyle) { this.changeListStyle(prefs.listStyle); }
      if (prefs.currentPage) { this.changePage(prefs.currentPage); }
      if (prefs.offset) { this.offset = prefs.offset; }
    },

    /**
     * Update preference with given key, prefixing it with the pref key first
     *
     * @param {Object} update - update that that should be merged with the existing preferences
     */
    updatePreference: function(update: object) {
      if (!this.prefSvc || !this.prefKey) { return Promise.reject(new Error("Missing/invalid preference service and/or prefKey")); }
      return this.prefSvc.updatePreference(this.prefKey, update);
    },

    changeListStyle: function(style: string) {
      // Ensure style is valid
      if (!VALID_LIST_STYLES.includes(style)) { throw new Error(`Invalid list style ${style}`); }
      this.listStyle = style;

      if (this.prefKey) { this.updatePreference({listStyle: this.listStyle}); }
    },

    changePage: function() {
      if (this.isServerSide) {
        this.changePageServer(...arguments);
      } else {
        this.changePageLocal(...arguments);
      }

      if (this.prefKey) {
        this.updatePreference({currentPageNumber: this.currentPageNumber});
        this.updatePreference({offset: this.offset});
      }
    },

    changePageLocal: function(p: number) {
      var newOffset = (p * this.currentPageNumber) - this.pageSize;

      // Ensure we don't attempt to set a new offset past the ned of the list
      if (isNullOrUndefined(this.data) || newOffset > this.data.length) { return; }

      this.offset = newOffset;
      this.currentPageNumber = p;
    },

    changePageServer: function(p: number) {
      var newOffset = (p * this.pageSize) - this.pageSize;
      this.fetchFn(this.generateQueryParams({offset: newOffset, pageSize: this.pageSize}));
    },

    /**
     * Generate query parameters fit to send
     *
     * @param {object} override
     * @param {number} override.offset
     * @param {number} override.pageSize
     */
    generateQueryParams: function(override={}) {
      var current = { offset: this.offset, pageSize: this.pageSize };
      var queryParams = Object.assign({}, current, override);

      return this.getParamTransformFn ? this.getParamTransformFn(queryParams) : queryParams;
    },

    /**
     * Fetch listing data
     *
     * @param {object} searchParams - Search parameters
     * @param {number} searchParams.offset - Offset
     * @param {number} searchParams.pageSize - Size of the pages to return
     */
    fetchListingData: function(searchParams: SearchParams) {
      searchParams = searchParams || {offset: this.offset, pageSize: this.pageSize};

      this.requestInFlight = true;
      let url: string = this.url;

      // Generate the URL
      if (url && this.urlFn) { url = this.urlTransformFn(url); }

      let res;

      if (!this.reqFn) {
        throw new Error("no request function provided");
      }

      res = this.reqFn(url, searchParams);
      if (!(res instanceof Promise)) { throw new Error("Invalid Request Fn, it does not produce a promise"); }

      // At this point, res should evaluate to an object that contains {items: [...], count: x, total: y }
      // 'total' key takes priority
      res
        .then(resp =>  {
          // Allow for either 'count' or 'total'
          this.totalCount = resp.total || resp.count;
          return resp.items;
        })
        .then(items => {
          // Update offset/current page #
          this.offset = searchParams.offset;
          this.currentPageNumber = (this.offset / searchParams.pageSize) + 1;

          this.requestInFlight = false;
          this.data = items;
        })
        .catch(err => {
          this.requestInFlight = false;
          // Rethrow the error
          throw err;
        });
    }

  }
};
