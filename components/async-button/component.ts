export default {
  props: {
    // Determine if the states will be controlled manually (by updating `state` prop)
    manual: Boolean,
    state: String,

    defaultText: {type: String, default: ""},
    loadingText: {type: String, default: "Loading..."},
    errorText: {type: String, default:"Failure"},
    successText: {type: String, default: "Success"},

    reactivateWaitAfterSuccessMs: {type: Number, default: 2000},
    // whether to do the async action only once (if successful, never allow it to be done again)
    onlyOnce: Boolean
  },

  data: function() {
    if (this.manual && !this.state) {
      throw new Error("Manually controlled async button must have `state` prop specified");
    }

    // Ensure the button is at least as big as it's biggest type of text
    var maxPhraseWidth = Math.max(...[this.defaultText.length, this.loadingText.length, this.successText.length]);

    return {
      loading: false,
      error: false,
      success: false,
      textMinWidth: maxPhraseWidth + "rem"
    };
  },

  computed: {
    isLoading: function() { return this.manual ? this.state === "loading" : this.loading; },
    isError: function() { return this.manual ? this.state === "error" : this.error; },
    isSuccess: function() { return this.manual ? this.state === "success" : this.success; }
  },

  methods: {
    /**
     * Wrap the click listener (if it exists)  in a method that will run it,
     * and update the state of this button as necessary
     */
    handleClick: function() {
      if (!this.$listeners || !this.$listeners.click) {
        return;
      }

      // Call the click invoker
      var clickResult = this.$listeners.click();

      // If the result isn't a promise, do nothing
      if (!(clickResult instanceof Promise)) {
        return;
      }

      // Do loading state setting
      this.loading = true;
      clickResult
        .then(() => {
          this.success = true;

          // Reactivate after success
          if (!this.onlyOnce) {
            setTimeout(() => this.success = false, this.reactivateWaitAfterSuccessMs);
          }

          this.loading = false;
        }, () => {
          this.error = true;
          this.loading = false;
        });
    }

  }
};
