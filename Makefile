.PHONY: all install \
				build build-ts build-ts-watch reqs \
				lint lint-watch \
				tests test-unit test-int test-e2e \
				serve-demo

all: install lint build
test: test-unit test-int test-e2e

LOCAL_WEB_PORT=3000

MAKEFILE_DIR ?= $(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))
NPM ?= npm
YARN ?= yarn
ENTR ?= entr

#################
# Dev Utilities #
#################

dev-env-setup:
	cp ./.dev-util/git/hooks/pre-push.sh .git/hooks/pre-push
	chmod +x .git/hooks/pre-push

dev-env-teardown:
	rm .git/hooks/pre-push

######################
# Code quality/Tests #
######################

lint:
	$(YARN) lint

lint-watch:
	find . -name "*.ts" | $(ENTR) -rc $(YARN) lint

test-unit: build-ts
	$(YARN) test-unit

test-int: build-ts
	$(YARN) test-int

test-e2e: build-ts
	$(YARN) test-e2e

#########
# Build #
#########

install:
	$(YARN) install

build:
	$(YARN) parcel build components/alert-notification/component.vue -d components/alert-notification -o index
	$(YARN) parcel build components/alert-listing/component.vue -d components/alert-listing -o index
	$(YARN) parcel build components/async-button/component.vue -d components/async-button -o index
	$(YARN) parcel build components/custom-option-select/component.vue -d components/custom-option-select -o index
	$(YARN) parcel build components/dashboard-text-widget/component.vue -d components/dashboard-text-widget -o index
	$(YARN) parcel build components/generic-listing/component.vue -d components/generic-listing -o index
	$(YARN) parcel build components/listing-table-item/component.vue -d components/listing-table-item -o index
	$(YARN) parcel build components/loading-indicator/component.vue -d components/loading-indicator -o index
	$(YARN) parcel build components/panel-container/component.vue -d components/panel-container -o index
	$(YARN) parcel build components/password-change-form/component.vue -d components/password-change-form -o index
	$(YARN) parcel build components/printed-address/component.vue -d components/printed-address -o index
	$(YARN) parcel build components/tag-listing-item/component.vue -d components/tag-listing-item -o index
	$(YARN) parcel build components/tag-toggle-selector/component.vue -d components/tag-toggle-selector -o index
	$(YARN) parcel build index.ts -d .

build-ts:
	$(YARN) build-ts

build-ts-watch:
	$(YARN) build-ts-watch

# Starts local server with no cache
serve-demo:
	$(YARN) serve-demo
