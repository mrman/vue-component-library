import Vue from "vue";

import "purecss/build/pure-min.css";
import "purecss/build/grids-responsive-min.css";
import "font-awesome/css/font-awesome.min.css";

// All-in-one import
import "../index";

import {
  Address,
  Company,
  Job,
  Stringable,
} from "../types";

const MOCK_ADDRESS: Address = {
  id: 0,
  line1: "123 Barton Springs Rd",
  line2: "Apt 123",
  zipCode: "78703",
  town: "Austin",
  state: "TX",
  country: "USA"
};

const MOCK_COMPANY: Company = {
  id: 0,
  name: "Mock Company",
  description: "Not a real company",
  cultureDescription: "We're fake!",
  homepageUrl: "https://www.example.org",
  iconUri: null,
  bannerImageUri: "https://www.example.org/something.png",
  addressId: 0,
  primaryColorCSS: "gray",
  createdAt: new Date(),
  lastUpdatedAt: new Date()
};

const MOCK_JOB: Job = {
  id:  0,
  title:  "Job Title: Fake job",
  description:  "Job description - 'This isn't a real job'",
  industry:  "IT",
  type:  "Contract",
  minSalary:  4000,
  maxSalary:  6000,
  salaryCurrency:  "USD",
  postingDate:  new Date(),
  employerId:  0,
  isActive:  true,
  tags: [
    {
      name: "JS",
      cssColor: "green",
      names: {"EN_US":"JS","EN_JP":"JS"},
      createdAt: new Date()
    },
    {
      name: "HTML",
      cssColor: "orange",
      names: {"EN_US":"HTML","EN_JP":"HTML"},
      createdAt: new Date()
    }
  ],
  applyLink:  "https://example.org/job",
  urlBounceConfigId: 0
};

class MockAddressesSvc {
  getOrFetchAddressById(): Promise<Address> {
    return Promise.resolve(MOCK_ADDRESS);
  }
}

const MOCK_USER_SVC = new Vue({
  data: function(): object {
    return {
      state: {userIsLoggedIn: true},
      starredJobs: {}
    };
  },

  methods: {
    jobIsStarred: function(id: number): Promise<boolean> {
      return Promise.resolve(id in this.starredJobs);
    },

    unstarJob: function(id: number): void { this.starredJobs[id] = false; },
    starJob: function(id: number): void { this.starredJobs[id] = true; }
  }
});

const MOCK_JOBS_SVC = new Vue({});

// Create the app vue instance
export default Vue.extend({
  data: () => ({

    alerts: [
      {status: "info", message: "First alert -- specified in JS"},
      {status: "error", message: "Second alert -- also specified in JS"}
    ],

    options: [
      {label: "First Option", value: 1},
      {label: "Second Option", value: 2}
    ],
    preSelectedValue: 1,
    currentlySelectedOption: null,

    tableData: [
      {id: 0, name: "John Doe", occupation: "Clerk", hireDate: new Date()},
      {id: 1, name: "Jane Smith", occupation: "Software Engineer", hireDate: new Date()},
      {id: 2, name: "Charles Bigley", occupation: "Nurse", hireDate: new Date()}
    ],

    tableColumnDisplayCfg: [
      {title: "ID", field: "id"},
      {title: "User Name", field: "name"},
      {title: "Job", field: "occupation"},
      {title: "Date hired", field: "hireDate", formatter: (d: Stringable): string => d.toString() }
    ],

    tableFilterFields: ["name", "occupation", "id"],

    isLoading: false,

    panelContainerLHSCompressed: false,

    mockAddressesSvc: new MockAddressesSvc(),

    preSelectedTag: [0],

    tags: [
      {name: "First", value: 0},
      {name: "Second - preselected", value: 1},
      {name: "Third", value: 2}
    ],

    uploadedImage: null,

    mockCompany: MOCK_COMPANY,
    mockJobsSvc: MOCK_JOBS_SVC,
    mockUserSvc: MOCK_USER_SVC,
    mockJob: MOCK_JOB,

    mockEnumTranslations: {
      job: {
        type: {
          "FullTime": "Full Time",
          "PartTime": "Part Time",
          "Freelance": "Freelance",
          "Contract": "Contract"
        }
      }
    }
  }),

  methods: {
    wait3Seconds: function(): Promise<void> {
      return new Promise(resolve => setTimeout(() => resolve(), 3000));
    },

    showLoadingIndicator: function(): Promise<void> {
      this.isLoading = true;
      return new Promise(resolve => setTimeout(() => {this.isLoading = false; resolve();}, 3000));
    },

    imageChanged: function(): Promise<void> {
      return Promise.resolve();
    }
  }

});
