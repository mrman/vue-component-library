/**
 * This filter converts an enum value to a user-fiendly printable string
 *
 * @param {string} value - The enum value to look up (ex. "FullTime")
 * @param {string} key - A prefix key to narrow conversion choise (ex. "job.type")
 * @param {Object} translations - A lookup of translations to use for the enum)
 */
export default function prettyEnum(value: string, translations: any, key: string): string {
  if (!(key in translations)) { return value; }
  const translationsByKey: any = translations[key];
  if (!(value in translationsByKey)) { return value; }
  return translationsByKey[value];
}
