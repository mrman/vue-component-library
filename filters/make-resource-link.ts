/**
 * This filter converts an string (ex. a resource ID like 1234) to a site link
 *
 * @param {string} suffix - The final part of the URL
 * @param {string} resourcePath - The path to the resource (after base URL and before suffix)
 * @param {string} [siteUrl] - The absolute site URL to use (possibly defaulting to window.location.origin)
 */
export default function makeResourceLink(suffix: string, resourcePath: string, siteUrl: string): string {
  siteUrl = siteUrl || window.location.origin;
  return `${siteUrl}/${resourcePath}/${suffix}`;
}
