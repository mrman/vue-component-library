import { isNullOrUndefined } from "../util";

/**
 * This filter converts an enum value to a user-fiendly printable string
 *
 * @param {string} value - The enum value to look up (ex. "FullTime")
 * @param {number} maxLength - Maximum length of the text
 */
export default function truncate(value: string, maxLength: number): string {
  if (isNullOrUndefined(value)) { return ""; }
  return value.length && value.length >= maxLength ? `${value.slice(0,maxLength - 3)}...` : value;
}
