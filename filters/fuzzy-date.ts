import { default as moment, Moment } from "moment";

/**
 * This filter converts an javascript date value to a fuzzy date (with the help of Moment.js)
 *
 * @param {Date} date - The value to convert to a fuzzy date
 */
export default function fuzzyDate(date: Date): string {
  const m: Moment = moment(date);
  if (!m.isValid()) { return "N/A (bad date)"; }
  return m.fromNow();
}
