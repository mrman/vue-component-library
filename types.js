"use strict";
exports.__esModule = true;
var ResponseStatus;
(function (ResponseStatus) {
    ResponseStatus["success"] = "success";
    ResponseStatus["error"] = "error";
})(ResponseStatus = exports.ResponseStatus || (exports.ResponseStatus = {}));
//# sourceMappingURL=types.js.map