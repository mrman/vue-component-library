# Vue Component Library

In this repo sleep some shared Vue components that can be used across webapps

# To include in a project

Install vue-component-library (this project) from the configured repository

``` bash
jspm install gitlab:mrman/vue-component-library
```

# Getting Started #

## Set up the dev environment (enables githooks, etc) ##

`make dev-env-setup`

## Retrieve requirements (npm, jspm) ##

`make reqs`

## Run the local server that will serve the component demo page ##

`make serve-demo-page`
