export function isNullOrUndefined(obj: any) {
  if (typeof obj === 'undefined') { return true; }
  if (obj === null) { return true }
  return false;
}
