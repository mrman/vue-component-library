#!/bin/env bash

LINT_CMD="make lint"
eval $LINT_CMD
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "LINT Failed!\n CMD: $LINT_CMD"
    exit 1
fi

UNIT_TESTS_CMD="make test-unit test-int"
eval $UNIT_TESTS_CMD
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "Unit + Integration tests failed!\n CMD: $UNIT_TESTS_CMD"
    exit 1
fi
